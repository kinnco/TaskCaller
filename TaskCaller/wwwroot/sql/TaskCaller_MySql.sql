/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.110
Source Server Version : 80012
Source Host           : 192.168.1.110:3366
Source Database       : TaskCaller

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2019-03-01 13:46:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for DelayTask
-- ----------------------------
DROP TABLE IF EXISTS `DelayTask`;
CREATE TABLE `DelayTask` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL DEFAULT '',
  `Url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `PostData` varchar(8000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `TriggerTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `MaxRetryCount` int(11) NOT NULL DEFAULT '3',
  `RetrySeconds` int(11) NOT NULL DEFAULT '5',
  `ExecCount` bigint(11) NOT NULL DEFAULT '0',
  `TimeoutSeconds` int(11) NOT NULL DEFAULT '15',
  `SuccessFlag` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `AddTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Enable` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ExecuteLog
-- ----------------------------
DROP TABLE IF EXISTS `ExecuteLog`;
CREATE TABLE `ExecuteLog` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `TaskId` bigint(20) NOT NULL,
  `TaskName` varchar(100) NOT NULL,
  `TaskUrl` varchar(500) NOT NULL,
  `TaskType` int(11) NOT NULL,
  `TaskMethod` varchar(10) NOT NULL,
  `PostData` varchar(8000) NOT NULL,
  `Status` int(11) NOT NULL,
  `Message` varchar(8000) NOT NULL,
  `AddTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`),
  KEY `tt` (`TaskId`,`TaskType`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for TimedTask
-- ----------------------------
DROP TABLE IF EXISTS `TimedTask`;
CREATE TABLE `TimedTask` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Cron` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `PostData` varchar(8000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `ExecCount` bigint(20) NOT NULL DEFAULT '0',
  `LastExecTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastStatus` int(11) NOT NULL DEFAULT '0',
  `TimeoutSeconds` int(11) NOT NULL DEFAULT '0',
  `Enable` bit(1) NOT NULL DEFAULT b'0',
  `AddTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SuccessFlag` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
