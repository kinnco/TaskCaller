﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskCaller.Models
{
    public class ConnectionStringsConfig
    {
        public string SqlServer { get; set; }
        public string MySql { get; set; }
    }
}
